<?php
 
 $nano = True;
 while (  (!(json_decode(file_get_contents("php://input"),true))) && ($nano)  )
 {
 	$nano = time_nanosleep(0,125000000);
 }

 $input = json_decode(file_get_contents("php://input"),true);
 
 $sentiment = ['good','bad'];
 if(isset($input['search'])) {
	 $tweets = [
		[
			'id' => '849843003783090177',
			'user' =>	'VABVOX',
			'sentiment' => $sentiment[array_rand($sentiment)]	
		],
		[
			'id' => '849840458817708032',
			'user' =>	'mcspocky',
			'sentiment' => $sentiment[array_rand($sentiment)]	
		],
		[
			'id' => '849822615858499584',
			'user' =>	'sjredmond',
			'sentiment' => $sentiment[array_rand($sentiment)]	
		]
	 ];
	 shuffle($tweets);
	 echo json_encode($tweets);
 }
?>