#include "sentiment_analysis.h"

int main(int argc, char* argv[])
{
    if (argc < 2) {
        fprintf(stderr, "You should really pass a parameter in quotes.\n");
        return 1;
    }
    float s = getSentiment(argv[1]);
    printf("I'm feeling very (%lf) about this.\n", s);
}
