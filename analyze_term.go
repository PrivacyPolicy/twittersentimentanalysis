// NATE will write this

package twittersentiment //? or main?

import (
    "fmt"
)

type Tweet struct {
    text, user string
    retweets, likes int
}

/*
 * term: a string which is the search term entered in the search box
 * return: a list of sentiments values (numbers between -1 and 1, representing
 * bad and good, respectively).
 *     Ex: [-1, 1, 0, -0.1, 0.2, 0.65, ...]
 */
func analyze_term(term string) {
    // TODO
    // initialize and run MPI code
    //     tweetList := "get_tweets.go".getTweets(term) // [Tweet{}, Tweet{}, ...]
    //     sentiments := []
    //     for tweet in tweetList {
    //         sentiment := "sentiment_analysis.go".getSentiment(tweet.text)
    //         sentiments.append(sentiment)
    //     }
    //     return sentiments // ???
    // end MPI code
    // return all_sentiments_somehow // ??? Access it from a file???

    // you may choose to refrain from doing this in MPI at first, and just run
    // it all concurrently. We can figure out the MPI stuff later if need be.
}

func main() {
    // TODO
    // test the code here
}
