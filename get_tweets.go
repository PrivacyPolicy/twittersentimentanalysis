// BRIAN will write this

package twittersentiment //?

import (
    "fmt"
)

type Tweet struct {
    text, user string
    retweets, likes int
}

/*
 * term: a string which is the search term entered in the search box
 * min: an int that is the initial tweet to download/scrape
 * max: an int that is the final tweet to download/scrape
 * return: a list of tweet structs
 *     Ex: [Tweet{}, Tweet{}, ...]
 */
func getTweets(term string, min, max int) {
    // TODO
    // send a request to Twitter using the APIs that gets tweets using the
    // search term and only within the specified range (if possible)
    // return the list of tweet structs
}

func main() {
    // TODO
    // test the code here
}
