#include <Python.h>
#include <string>

#define PYFILENAME "sentiment"   // the python file being called
#define PYFUNCNAME "analyze"     // the python function being called

float getSentiment(std::string sentence);
