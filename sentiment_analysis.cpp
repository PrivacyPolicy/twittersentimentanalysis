#include "sentiment_analysis.h"

float getSentiment(std::string sentence)
{
    using namespace std;

    PyObject *pName, *pModule, *pDict, *pFunc, *pArgs, *pValue;
    int i;
    
    // Start python interpreter
    Py_Initialize();
    
    // Import python libraries
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append(\"./sentiment_analysis_files/\")");
    PyRun_SimpleString("from sentimentweb.info import feature_selection_trials, MyDict, classify2");
    
    // Load python file
    pName = PyString_FromString(PYFILENAME);
    if (pName == NULL) return 0;
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);
    
    if (pModule == NULL) {
        PyErr_Print();
        fprintf(stderr, "Could not load file \"%s\".", sentence.c_str());
        return 0;
    }
    
    // Get a reference to the python function
    pFunc = PyObject_GetAttrString(pModule, PYFUNCNAME);
    if (!pFunc || !PyCallable_Check(pFunc)) {
        if (PyErr_Occurred()) PyErr_Print();
        fprintf(stderr, "Cannot find function %s.\n", PYFUNCNAME);
        return 0;
    }
    
    // Prepare arguments for python call (actually kind of a pain)
    pArgs = PyTuple_New(1);
    pValue = PyString_FromString(sentence.c_str());
    if (!pValue) {
        Py_DECREF(pArgs);
        Py_DECREF(pModule);
        fprintf(stderr, "Cannot convert argument.\n");
        return 0;
    }
    
    // Moment of truth! Make python call
    PyTuple_SetItem(pArgs, 0, pValue);
    pValue = PyObject_CallObject(pFunc, pArgs);
    Py_DECREF(pArgs);
    if (pValue == NULL) {
        Py_DECREF(pFunc);
        Py_DECREF(pModule);
        PyErr_Print();
        fprintf(stderr, "Call failed.\n");
        return 0;
    }
    
    // Return the statistic value
    bool positive = PyTuple_GetItem(pValue, 0) == Py_True;
    double probability = PyFloat_AsDouble(PyTuple_GetItem(pValue, 1));
    // printf("Setniment is: %s. Probability is: %0.3f.\n", (positive) ? "pos" : "neg", probability);
    double d = ((positive) ? 1 : -1) * probability;
    if (d > 1) d = 1;
    if (d < -1) d = -1;
    Py_DECREF(pValue);
    return d;
}
